#include <sys/types.h>
#include <dirent.h>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstring>
#include <fstream>

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		std::cerr << "Usage: " << argv[0] << " <search string>" << std::endl;
		exit(1);
	}

	DIR *stream = opendir("./");

	struct dirent *currDir;
	struct stat st_buf;
	pid_t childPID;

	std::string expression = argv[1];
	std::string path = "./";

	int childStatus;

	int numChildren = 0;

	while(currDir = readdir(stream))
	{
		int status = lstat(currDir->d_name, &st_buf);

		if(status != 0)
		{
			std::cout << "Error: " << strerror(errno)<< std::endl;
			std::cout << "Error: " << childPID << std::endl;
			std::cout << "Error: " << currDir->d_name << std::endl;
			exit(1);
		}

		std::string name = currDir->d_name;

		if(S_ISREG(st_buf.st_mode))
		{
			std::ifstream iFile(name);
			if( iFile.is_open() )
			{
				std::string line = "";

				while( std::getline( iFile, line) )
				{
					if(line.find(expression) != std::string::npos)
					{
						std::cout << "Found " << expression << " in file: " <<  path << name << std::endl;
						break;
					}
				}
				iFile.close();
			}

		}
		else if(S_ISDIR(st_buf.st_mode) && name != "." && name != "..")
		{
			numChildren++;
			childPID = fork();

			if(childPID == -1)
			{
				std::cerr << "Fork failed! Exiting...\n";
				exit(1);
			}

			if(childPID == 0) // we are the child
			{
				numChildren = 0;
				std::string dirName = currDir->d_name;
				if(dirName != "." && dirName != "..")
				{
					path += dirName + "/";
					chdir(dirName.c_str());
					stream = opendir("./");
				}
			}
		}
	}

	while(numChildren > 0)
	{
		wait(&childStatus);
		numChildren--;
	}


	return 0;
}