CXX = g++
INCLUDE = -I./include
CXXFLAGS = $(INCLUDE) -O2 -pipe -std=c++11

EXEC = DirectorySearch

all: $(EXEC)


$(EXEC): 
	$(CXX) $(CXXFLAGS) -o $(EXEC) src/$(EXEC).cpp

clean:
	rm $(EXEC)